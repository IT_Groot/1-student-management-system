#include <stdio.h>
#include "StudentSystem.h"

void UpdataStudent(stu* head)
{
	int a;
	printf("\n修改全部学生信息请输入1\n");
	printf("\n修改学生姓名请输入2\n");
	printf("\n修改学生学号请输入3\n");
	printf("请输入您想操作的号码:>");
	scanf("%d", &a);

	switch(a)
	{
		case 1:
		{
			UpAll(head);	
		}
		break;
		case 2:
		{
			UpName(head);
		}
		break;
		case 3:
		{
			UpId(head);
		}
		break;
	}
	
}

void StudentInfo(stu*A)
{
	printf("\n修改前的学生信息为:\n");
	printf("\n学生信息:\n \t姓名 \t年龄 \t性别 \t专业 \t学号 \t成绩 \n");
	printf("%s", A->name);
	printf("%hd", A->age);
	printf("%s", A->sex);
	printf("%s", A->major);
	printf("%s", A->id);
	printf("%d", A->score);
	printf("\n");
}

void UpName(stu* head)
{
	stu *p, *q;
	char Upname[31];
	p = head;
	q = p->next;
	
	printf("\n请输入您想修改学生的姓名:>");
	scanf("%s", Upname);
	while(q != NULL)
	{	
		if(strcmp(q->name, Upname) == 0)
		{
			StudentInfo(q);	
			printf("\n请输入修改后的姓名:>");
			scanf("%s", q->name);
			printf("\n修改成功！");
		}
		
		q = q->next;
		
	}
}

void UpId(stu* head)
{
	stu *p, *q;
	char Upid[20];
	p = head;
	q = p->next;
	
	printf("\n请输入您想修改学生的学号:>");
	scanf("%s", Upid);
	while(q != NULL)
	{	
		if(strcmp(q->id, Upid) == 0)
		{
			StudentInfo(q);
			printf("\n请输入修改后的学号:>");
			scanf("%s", q->id);
			printf("\n修改成功！");
		}
		
		q = q->next;
	}
}

void UpAll(stu* head)
{
	stu *p, *q;
	char Upid[31];
	p = head;
	q = p->next;
	
	printf("\n请输入您想修改学生的学号:>");
	scanf("%s", Upid);
	while(q != NULL)
	{	
		if(strcmp(q->id, Upid) == 0)		
		{
			printf("\n请输入更改后的学生姓名:>");
			scanf("%s", q->name);
	
			printf("\n请输入更改后的学生年龄:>");
			scanf("%hd", &q->age);
			while(q->age < 0)
			{
				printf("\n请检查是否输入错误，请重新输入！\n");
				scanf("%d", &q->age);
			}
				
			printf("\n请输入更改后的学生性别:>");
			scanf("%s", q->sex);
			while(strcmp(q->sex,"男")!= 0 && strcmp(q->sex,"女") !=0 )
			{
				printf("\n输入格式有误，请重新输入: (输入男或女!)\n");
				scanf("%s",q->sex);
			}
			
			printf("\n请输入更改后的学生专业:>");
			scanf("%s", q->major);
			printf("\n请输入更改后的学生学号:>");
			scanf("%s", q->id );
			printf("\n请输入更改后的学生成绩:>");
			scanf("%d", &q->score);
			while(q->score < 0)
			{
				printf("请检查是否输入错误，请重新输入！\n");
				scanf("%d", &q->score);
			}	
			printf("\n更改成功！\n");
		}	
		q = q->next;
	}

}
