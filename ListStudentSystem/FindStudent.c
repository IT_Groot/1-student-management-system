#include <stdio.h>
#include "StudentSystem.h"

void FindStudent(stu* head)
{
	int a;
	printf("查看所有学生信息请按1\n");
	printf("按照学生姓名查找请按2\n");
	printf("按照学生学号查找请按3\n");
	printf("请输入查询模式:>\n");
	scanf("%d", &a);
	
	switch(a)
	{
		case 1:
		{
			AllFind(head);	
		}
		break;
		case 2:
		{
			NameFind(head);
		}
		break;
		case 3:
		{
			IdFind(head);
		}
		break;
		default:
		{
			printf("\n您输入的编号有误");
		}
		break;
	}
}

void AllFind(stu* head)
{
	stu *p, *q;
	p = head;
	q = p->next;
	printf("\n学生信息:\n \t姓名 \t年龄 \t性别 \t专业 \t学号 \t成绩 ");
	while(q != NULL)
	{
		printf("%s", q->name);
		printf("%hd", q->age);
		printf("%s", q->sex);
		printf("%s", q->major);
		printf("%s", q->id);
		printf("%d", q->score);
		printf("\n");
		q = q->next;	
	}		
}

void NameFind(stu* head)
{
	stu *p, *q;
	p = head;
	q = p->next;
	char findname[31];
	printf("请输入您想查找学生的姓名:>");
	scanf("%s", findname);
	while(q != NULL)
	{
		if(strcmp(q->name, findname) == 0)
		{
			printf("\n学生信息:\n \t姓名 \t年龄 \t性别 \t专业 \t学号 \t成绩 ");
			printf("%s", q->name);
			printf("%hd", q->age);
			printf("%s", q->sex);
			printf("%s", q->major);
			printf("%s", q->id);
			printf("%d", q->score);
			printf("\n");
		}
		else
		{
			printf("\n没有该学生信息\n");
		}
		p = q;	
		q = p->next;
	}
	
}

void IdFind(stu* head)
{
	stu *p, *q;
	p = head;
	q = p->next;
	char findid[20];
	printf("请输入您想查找学生的学号:>");
	scanf("%s", findid);
	while(q != NULL)
	{
		if(strcmp(q->id, findid) == 0)
		{
			printf("\n学生信息:\n \t姓名 \t年龄 \t性别 \t专业 \t学号 \t成绩 ");
			printf("%s", q->name);
			printf("%hd", q->age);
			printf("%s", q->sex);
			printf("%s", q->major);
			printf("%s", q->id);
			printf("%d", q->score);
			printf("\n");
		}
		else
		{
			printf("\n没有该学生信息\n");
		}
		p = q;
		q = p->next;
	}

}
