#include <stdio.h>
#include "StudentSystem.h"

void Save(stu* head) 
{
	FILE *fp = fopen("StudentInfo.txt","w");
	stu *p=head;
	p= p->next;
	fprintf(fp,"学生信息:\n\t姓名 \t年龄 \t性别 \t专业 \t学号 \t成绩\n");
	while(p != NULL)
 	{
 		fprintf(fp,"\t%s %hd %s %s %s %d\n", p->name, p->age, p->sex, p->major, p->id, p->score);
		p = p->next;
	}
	fclose(fp);
	printf("\n保存成功！\n");
}
