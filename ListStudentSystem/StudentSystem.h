#ifndef _StudentSystem_H
#define _StudentSystem_H

/*----------结构体定义区域----------*/
typedef struct student
{
	char name[31];		//姓名
	short age;		//年龄
	char sex[5];		//性别
	char major[31];		//专业
	char id[20];		//学号
	int score;		//成绩
	struct student* next;	//结构体指针	
}stu;


/*----------函数定义区域----------*/
//增加学生信息函数
void AddStudent(stu* head);
//删除学生信息函数
void DeleteStudent(stu* head);
//打印学生信息函数
void StudentInfo(stu* A);
//修改学生信息函数
void UpdataStudent(stu* head);
//修改学生全部信息
void UpAll(stu* head);
//修改学生姓名
void UpName(stu* head);
//修改学生学号
void UpId(stu* head);
//查找学生信息函数
void FindStudent(stu* head);
//查找学生全部信息
void AllFind(stu* head);
//姓名查找学生信息
void NameFind(stu* head);
//学号查找学生信息
void IdFind(stu* head);
//保存学生信息函数
void Save(stu* head);
#endif
