#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "StudentSystem.h"

void AddStudent(stu* head)
{
	stu *p, *q;
	int num, i;
	p = head;
	q = p->next;
	
	printf("\n请输入您想增加学生的个数:>");
	scanf("%d", &num);
	for(i = 0; i < num; i++)
	{
		stu* A = (stu*)malloc(sizeof(stu));
		
		printf("\n请输入学生姓名:>");
		scanf("%s", A->name);
		
		printf("\n请输入学生年龄:>");
		scanf("%hd", &A->age);
		while(A->age <= 0)
		{
			//判断年龄是否小于0
			printf("\n年龄输入有误，请重新输入!\n");
			scanf("%hd", &A->age);
		}
		
		printf("\n请输入学生性别:>");
		scanf("%s", A->sex);
		while(strcmp(A->sex,"男") != 0 && strcmp(A->sex,"女") != 0)
		{
			//判断性别是否是男女
			printf("\n性别输入有误，请重新输入！\n");
			scanf("%s", A->sex);
		}
		
		printf("\n请输入学生专业:>");
		scanf("%s", &A->major);
		
		printf("\n请输入学生学号:>");
		scanf("%s", A->id);
		
		printf("\n请输入学生成绩:>\n");
		scanf("%d",&A->score );
		while(A->score < 0) 
		{        
			//判断成绩是否小于0
			printf("请检查是否输入错误，请重新输入！\n");
			scanf("%d",&A->score );
		}
	
		p->next = A;
		p = p->next;
		A->next = NULL;
		
		printf("\n 添加成功！\n");
	}
	p->next = NULL;
	return;
}
