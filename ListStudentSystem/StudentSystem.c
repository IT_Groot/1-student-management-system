#include <stdio.h>
#include <stdlib.h>
#include "StudentSystem.h"
 
int main()
{
	stu *head=(stu*)malloc(sizeof(stu));
	head->next = NULL;
	short a;
	while(1) 
	{
		printf("\t\t\t\t\t**************************\n");
		printf("\t\t\t\t\t*  欢迎使用学生管理系统  *\n");
		printf("\t\t\t\t\t*  请选择想要使用的功能  *\n");
		printf("\t\t\t\t\t*    添加学生信息请按1   *\n");
		printf("\t\t\t\t\t*    查看学生信息请按2   *\n");
		printf("\t\t\t\t\t*    按照成绩排序请按3   *\n");
		printf("\t\t\t\t\t*    更改学生信息请按4   *\n");
		printf("\t\t\t\t\t*    删除学生信息请按5   *\n");
		printf("\t\t\t\t\t*    保存学生信息请按6   *\n");
		printf("\t\t\t\t\t*      退出系统请按0     *\n");
		printf("\t\t\t\t\t**************************\n");
		scanf("%hd",&a);
		switch(a) 
		{
			case 1:
			{
				system("clear");
				AddStudent(head);
			}
			break;
			case 2:
			{
				system("clear");
				FindStudent(head);
			}
			break;
			case 3:
			{		
				system("clear");
				BubbleSort(head);
			}
			break;
			case 4:
			{
				system("clear");
				UpdataStudent(head);
			}	
			break;
			case 5:
			{
				system("clear");
				DeleteStudent(head);
			}
			break;
			case 6:
			{
				system("clear");
				Save(head);
				break;
			}
			case 0:
			{
				printf("\n感谢使用本系统！期待下次与你相见!\n");
				exit(0);
			}
			break;
			
			default:
			{
				system("clear");
				printf("输入方式错误，请重新输入！\n");
				break;
			}
		}
	}
	return 0;
}

